
package esagitexercise;


public class ESAGitExercise {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Greetings theGreeter = new Greetings("Catherine");
        theGreeter.sayHi();
        theGreeter.sayWelcome();
        theGreeter.sayGoodbye();
    }
    
}
